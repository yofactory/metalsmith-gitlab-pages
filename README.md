﻿# generator-metalsmith-gitlab-pages

_Yeoman Generator, metalsmith-gitlab-pages, for a Metalsmith static website (in Typescript) for GitLab Pages._



This generator creates a simple Metalsmith website skeleton (in typescript) that can be used for GitLab Pages.



## Installation

First, install [Yeoman](http://yeoman.io) and [generator-metalsmith-gitlab-pages](https://www.npmjs.com/package/generator-metalsmith-gitlab-pages) using [npm](https://www.npmjs.com/).
(You'll obviously need [node.js](https://nodejs.org/) installed on your system. Bower is optional.).

```bash
npm install -g yo typescript
npm install -g generator-metalsmith-gitlab-pages
```

Then generate your new project:

```bash
mkdir my-metalpage-project
cd my-metalpage-project
yo metalsmith-gitlab-pages
```


## Local Manual Build

```bash
make build
```


## GitLab CI/CD

Navigate to your Project's Settings | Pipelines, and
enable Shared Runners.





## License

MIT © [Harry Y](https://gitlab.com/yofactory)




