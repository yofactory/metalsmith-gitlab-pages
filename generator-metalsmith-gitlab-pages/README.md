﻿# generator-metalsmith-gitlab-pages [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Yeoman generator for simple metalsmith static website (in typescript) for GitLab Pages.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-metalsmith-gitlab-pages using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo typescript
npm install -g generator-metalsmith-gitlab-pages
```

Then generate your new project:

```bash
yo metalsmith-gitlab-pages
```



## Local Manual Build

You can build your website using `make`:

```bash
make build
```

The static website output is created in the `public` folder, which is used for GitLab Pages.
Generally, you can customize your website by modifying the content files under the `input` folder and/or the layout files under the `layouts` folder.
You can also update the `src/index.ts` file for further customization.



## GitLab CI/CD

Navigate to your Project's Settings | CI/CD | Runners settings, and
enable Shared Runners.
(Note: It seems that Shared Runners are automatically enabled when you create a new project.)

Now, whenever you check in a change, the build will run,
and your project website will be automatically updated.




## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Harry](https://gitlab.com/realharry)


[npm-image]: https://badge.fury.io/js/generator-metalsmith-gitlab-pages.svg
[npm-url]: https://npmjs.org/package/generator-metalsmith-gitlab-pages
[travis-image]: https://travis-ci.org/harrywye/generator-metalsmith-gitlab-pages.svg?branch=master
[travis-url]: https://travis-ci.org/harrywye/generator-metalsmith-gitlab-pages
[daviddm-image]: https://david-dm.org/harrywye/generator-metalsmith-gitlab-pages.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/harrywye/generator-metalsmith-gitlab-pages
